import Alpine from "alpinejs";

window.Alpine = Alpine;

Alpine.data("j2mc", () => {
  return {
    navActive: false,
    darkActive: false,
    message: {
      firstName: "",
      lastName: "",
      emailAddress: "",
      phoneNumber: "",
      bodyMessage: "",
    },
    closeNav() {
      this.navActive = false;
    },
    toggleDark() {
      this.darkActive = !this.darkActive;
    },
    toggleNav() {
      this.navActive = !this.navActive;
    },
    sendEmail() {
      fetch("https://getform.io/f/563b40d4-4c6c-49f8-8b69-9b02c1a0fdac", {
        method: "POST",
        body: JSON.stringify(this.message),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      })
        .then((response) => response.json())
        .then((data) => console.log(data))
        .catch((error) => console.log(error));

      this.message = {};
    },
  };
});

window.Alpine.start();
